package services

import (
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

type ArticleServiceInMemory struct {
	Repository *repository.ArticleStorage
}

func NewArticleService(repo *repository.ArticleStorage) model.PostService {
	return &ArticleServiceInMemory{
		Repository: repo,
	}
}

func (a *ArticleServiceInMemory) CheckSlugIsExist(slug string) bool {
	_, err := a.Repository.FindBySlug(slug)
	if err != nil {
		return false
	}

	return true
}

func (a *ArticleServiceInMemory) GetPublishedArticleBySlug(slug string) (model.Article, error) {
	article, err := a.Repository.FindBySlug(slug)
	if err != nil {
		return model.Article{}, err
	}
	return article, err
}

func (a *ArticleServiceInMemory) GetPublishedArticleByCategory(category model.Category) ([]model.Article, error) {
	articles, err := a.Repository.GetPublishedArticleByCategory(category)
	if err != nil {
		return nil, err
	}
	return articles, nil
}
