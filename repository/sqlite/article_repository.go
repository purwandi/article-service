package sqlite

import (
	"database/sql"

	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
)

type ArticleRepositorySQLite struct {
	DB *sql.DB
}

func NewArticleRepositorySQLite(DB *sql.DB) repository.ArticleRepository {
	return &ArticleRepositorySQLite{DB: DB}
}

func (repo *ArticleRepositorySQLite) Save(article *model.Article) error {
	statement, err := repo.DB.Prepare(`INSERT INTO ARTICLES(SLUG, NAME, BODY) VALUES(?,?,?)`)
	if err != nil {
		return err
	}

	statement.Exec(article.Slug, article.Name, article.Body)

	return nil
}

func (repo *ArticleRepositorySQLite) GetPublishedArticleByCategory(category model.Category) repository.QueryResult {
	result := repository.QueryResult{}
	articles := []model.Article{}
	rows, err := repo.DB.Query(`SELECT * FROM ARTICLES`)

	if err != nil {
		result.Error = err
	} else {
		for rows.Next() {
			article := model.Article{}
			rows.Scan(
				&article.Slug,
				&article.Name,
				&article.Body,
			)

			articles = append(articles, article)
		}

		result.Result = articles
	}

	return result
}

func (repo *ArticleRepositorySQLite) FindBySlug(slug string) repository.QueryResult {
	result := repository.QueryResult{}
	article := model.Article{}

	row := repo.DB.QueryRow(`SELECT * FROM ARTICLES WHERE SLUG = ?`, slug)

	err := row.Scan(
		&article.Slug,
		&article.Name,
		&article.Body,
	)

	if err != nil {
		result.Error = err
	} else {
		result.Result = article
	}

	return result
}
