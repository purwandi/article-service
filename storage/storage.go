package storage

import "gitlab.com/fannyhasbi/article-service/model"

// ArticleStorage is storage in memory
type ArticleStorage struct {
	ArticleMap map[string]model.Article
}

// CreateArticleStorage is to create article storage instance
func CreateArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: make(map[string]model.Article),
	}
}