package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"

	"gitlab.com/fannyhasbi/article-service/repository/sqlite"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/fannyhasbi/article-service/model"
	"gitlab.com/fannyhasbi/article-service/repository"
	"gitlab.com/fannyhasbi/article-service/repository/inmemory"
	"gitlab.com/fannyhasbi/article-service/storage"
)

func main() {
	driver := "sqlite"
	var repo repository.ArticleRepository

	switch driver {
	case "sqlite":
		repo = initSQLite()
	default:
		repo = initInmemory()
	}

	article := model.Article{
		Slug: "foobar",
		Name: "Foobar",
		Body: "Body fuubar",
	}

	repo.Save(&article)
	result := repo.FindBySlug("foobar")

	fmt.Println(result.Result.(model.Article))
}

func initInmemory() repository.ArticleRepository {
	storage := storage.CreateArticleStorage()

	return inmemory.NewArticleRepositoryInMemory(storage)
}

func initSQLite() repository.ArticleRepository {
	path := "database/sqlite/app.sqlite"
	db, err := sql.Open("sqlite3", path)

	if err != nil {
		panic(err)
	}

	// Run DDL
	ddl, err := ioutil.ReadFile("database/sqlite/ddl.sql")
	if err != nil {
		panic(err)
	}

	sql := string(ddl)

	_, err = db.Exec(sql)
	if err != nil {
		panic(err)
	}

	return sqlite.NewArticleRepositorySQLite(db)
}
